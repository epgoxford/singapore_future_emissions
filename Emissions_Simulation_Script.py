#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 14:58:49 2018

@author: aniqahsan
"""

######### Packages ############
#import csv
import scipy as sp
import pandas as pd
from numpy import *
from numpy.random import *
#import time
import scipy.stats
#import sys
import matplotlib.pyplot as plt
#import seaborn as sns
#import matplotlib


######### Files ############
primary_demand_file = r"/Users/aniqahsan/Documents/Oxford_PhD/Emissions Simulation/Scripts/data/IEA Singapore Primary Energy Demand.csv"
primary_demand_original_df = pd.read_csv(primary_demand_file)
######### Constants ############



####### Simulation Parameters to Set #########

USE_PEROVSKITE = 0 #change this value to run different parameter
USE_EXTERNAL = 1 #change this value to run different parameter

N_SOLAR_LIFE = 30#lifespan of solar panels, i.e. when to replace them
CURRENT_PV_EFFICIENCY = 0.16
# FINAL_PV_EFFICIENCY = 0.16#0.26

PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE = 0.01
PV_DEGREDATION_RATE = 0.005
MAX_AREA = 36.8#45 #km2
MAX_PV_CAPACITY = MAX_AREA * CURRENT_PV_EFFICIENCY * 1e-3# 10.0 #TWp assuming CURRENT_PV_EFFICIENCY
PVOUT = 1300 #kWh/kWp




LOW_T_HEAT_ELECTRIFICATION_L = 1
LOW_T_HEAT_ELECTRIFICATION_X1 = 4
LOW_T_HEAT_ELECTRIFICATION_X99 = 24

HIGH_T_HEAT_ELECTRIFICATION_L = 1
if USE_EXTERNAL == 1:
    HIGH_T_HEAT_ELECTRIFICATION_X1 = 7
    HIGH_T_HEAT_ELECTRIFICATION_X99 = 27
else:
    HIGH_T_HEAT_ELECTRIFICATION_X1 = 4
    HIGH_T_HEAT_ELECTRIFICATION_X99 = 24
    


TRANSPORT_ELECTRIFICATION_L = 1
TRANSPORT_ELECTRIFICATION_X1 = 4
TRANSPORT_ELECTRIFICATION_X99 = 24

ABSOLUTE_RE_PENETRATION_L = MAX_PV_CAPACITY * PVOUT#9.45# TWh/year maximum assuming all land area and current efficiency
ABSOLUTE_RE_PENETRATION_X1 = 0.1901100946607 #Value taken from s-curve fitting script
ABSOLUTE_RE_PENETRATION_X99 = 18.2089831177748 #Value taken from s-curve fitting script


#### Calculate Intermediate Parameters ########

if USE_PEROVSKITE == 1:
    FINAL_PV_EFFICIENCY = 0.26
else:
    FINAL_PV_EFFICIENCY = 0.16


# FRACTIONAL_RE_PENETRATION_L = 0#0.99#0
if USE_EXTERNAL == 1:
    FRACTIONAL_RE_PENETRATION_L = 0.99
else:
    FRACTIONAL_RE_PENETRATION_L = 0
FRACTIONAL_RE_PENETRATION_X1 = 9
FRACTIONAL_RE_PENETRATION_X99 = 29


LOW_T_HEAT_ELECTRIFICATION_L_array = array([0,0,0,1])
LOW_T_HEAT_ELECTRIFICATION_X1_array = array([LOW_T_HEAT_ELECTRIFICATION_X1] * 4)
LOW_T_HEAT_ELECTRIFICATION_X99_array = array([LOW_T_HEAT_ELECTRIFICATION_X99] * 4)


# HIGH_T_HEAT_ELECTRIFICATION_L_array = array([0,0,1,0])
if USE_EXTERNAL == 1:
    HIGH_T_HEAT_ELECTRIFICATION_L_array = array([0,0,1,1])
else:
    HIGH_T_HEAT_ELECTRIFICATION_L_array = array([0,0,1,0])
    
HIGH_T_HEAT_ELECTRIFICATION_X1_array = array([HIGH_T_HEAT_ELECTRIFICATION_X1] * 4)
HIGH_T_HEAT_ELECTRIFICATION_X99_array = array([HIGH_T_HEAT_ELECTRIFICATION_X99] * 4)

TRANSPORT_ELECTRIFICATION_L_array = array([0,1,0,1])
TRANSPORT_ELECTRIFICATION_X1_array = array([TRANSPORT_ELECTRIFICATION_X1] * 4)
TRANSPORT_ELECTRIFICATION_X99_array = array([TRANSPORT_ELECTRIFICATION_X99] * 4)


ABSOLUTE_RE_PENETRATION_L_array = array([ABSOLUTE_RE_PENETRATION_L] * 4)
ABSOLUTE_RE_PENETRATION_X1_array = array([ABSOLUTE_RE_PENETRATION_X1] * 4)
ABSOLUTE_RE_PENETRATION_X99_array = array([ABSOLUTE_RE_PENETRATION_X99] * 4)

FRACTIONAL_RE_PENETRATION_L_array = array([FRACTIONAL_RE_PENETRATION_L] * 4)
FRACTIONAL_RE_PENETRATION_X1_array = array([FRACTIONAL_RE_PENETRATION_X1] * 4)
FRACTIONAL_RE_PENETRATION_X99_array = array([FRACTIONAL_RE_PENETRATION_X99] * 4)

# case_name_list = ["BAU(a)", "Case 1(a)", "Case 2(a)", "Case 3(a)"]
if USE_EXTERNAL == 1:
    case_name_list = ["BAU(b)", "Case 1(b)", "Case 2(b)", "Case 3(b)"]
else:
    case_name_list = ["BAU(a)", "Case 1(a)", "Case 2(a)", "Case 3(a)"]

figure_name_list = ["(a)","(b)","(c)","(d)","(e)","(f)","(g)","(h)"]
# case_title = r"Single Junction; Without External RE"
if USE_PEROVSKITE == 1:
    case_title = r"Perovskite; "
else:
    case_title = r"Single Junction; "
if USE_EXTERNAL == 1:
    case_title = case_title + r"With External RE"
else:
    case_title = case_title + r"Without External RE"
# legend_loc = "lower right"
if USE_EXTERNAL == 1:
    legend_loc = "upper right"
else:
    legend_loc = "lower right"


######## Functions #################

def s_curve_fun(x, L, x1 , x99):
    x0 = (x1 + x99)/2
    k = log(99) / ((x99 - x1)/2)
    return L / (1 + exp(-k * (x - x0)))

s_curve_fun_vec = vectorize(s_curve_fun, excluded = ["L", "x1", "x99"])

def total_capacity_simulation_fun(N_LIFETIME, x, L, x1, x99):
    new_installations = s_curve_fun_vec(x = x, L = L, x1 = x1, x99 = x99) / (N_LIFETIME)
    total_installations = [0.0]
    for counter1 in range(N_LIFETIME):
        total_installations += [total_installations[-1] + new_installations[counter1]]
    for counter1 in range(size(x) - N_LIFETIME - 1):
        total_installations += [total_installations[-1] + new_installations[counter1 + N_LIFETIME] - new_installations[counter1]]
    return array(total_installations)


def growth_rate_calculation_fun(y_data):
    def fit_fun(x, a, b):
        return a + b * x
    x_data = arange(size(y_data))
    popt, pcov = sp.optimize.curve_fit(fit_fun, x_data, log(y_data))
    return exp(popt[1])

def primary_demand_prediction(primary_demand_original_df):
    #primary_demand_df = pd.DataFrame(data = ones((N_YEARS,4)), columns = ["electricity", "low_T_heat", "high_T_heat", "transport"])
    #primary_demand_df = pd.DataFrame(data = [list(primary_demand_original_df.iloc[-1])[1:]] * N_YEARS, columns = ["electricity", "low_T_heat", "high_T_heat", "transport"])
    electricity_growth_rate = growth_rate_calculation_fun(array(primary_demand_original_df[primary_demand_original_df.columns[1]])[20:])
    #electricity_growth_rate = 1.025
    low_t_heat_growth_rate = growth_rate_calculation_fun(array(primary_demand_original_df[primary_demand_original_df.columns[2]])[20:])
   #low_t_heat_growth_rate = 1.025
    high_t_heat_growth_rate = growth_rate_calculation_fun(array(primary_demand_original_df[primary_demand_original_df.columns[3]])[20:])
    #high_t_heat_growth_rate = 1.025
    #transport_growth_rate = growth_rate_calculation_fun(array(primary_demand_original_df[primary_demand_original_df.columns[4]])[20:])
    transport_growth_rate = 1.0
    original_demand = array(list(primary_demand_original_df.iloc[-1])[1:])
    electricity_demand = list(original_demand[0] * (electricity_growth_rate ** arange(N_YEARS)))
    low_t_heat_demand = list(original_demand[1] * (low_t_heat_growth_rate ** arange(N_YEARS)))
    high_t_heat_demand = list(original_demand[2] * (high_t_heat_growth_rate ** arange(N_YEARS)))
    transport_demand = list(original_demand[3] * (transport_growth_rate ** arange(N_YEARS)))
    new_demand = array([electricity_demand,low_t_heat_demand,high_t_heat_demand,transport_demand])
    primary_demand_df = pd.DataFrame(data = new_demand.T, columns = ["electricity", "low_T_heat", "high_T_heat", "transport"])
    return primary_demand_df


def electrification_simulation():
    electrification_df = pd.DataFrame(data = zeros((N_YEARS,4)) ,columns = ["electricity", "low_T_heat", "high_T_heat", "transport"])
    electrification_df["electricity"] = ones(N_YEARS)
    electrification_df["low_T_heat"] = s_curve_fun_vec(x = arange(N_YEARS), L = LOW_T_HEAT_ELECTRIFICATION_L, x1 = LOW_T_HEAT_ELECTRIFICATION_X1, x99 = LOW_T_HEAT_ELECTRIFICATION_X99) #linspace(0,1,N_YEARS) *1
    electrification_df["high_T_heat"] = s_curve_fun_vec(x = arange(N_YEARS), L = HIGH_T_HEAT_ELECTRIFICATION_L, x1 = HIGH_T_HEAT_ELECTRIFICATION_X1, x99 = HIGH_T_HEAT_ELECTRIFICATION_X99) #linspace(0,1,N_YEARS) *1
    electrification_df["transport"] = s_curve_fun_vec(x = arange(N_YEARS), L = TRANSPORT_ELECTRIFICATION_L, x1 = TRANSPORT_ELECTRIFICATION_X1, x99 = TRANSPORT_ELECTRIFICATION_X99) #linspace(0,1,N_YEARS) *1
    return electrification_df

def electrification_efficiency_simulation():
    electrification_efficiency_df = pd.DataFrame(data = zeros((N_YEARS,4)) ,columns = ["electricity", "low_T_heat", "high_T_heat", "transport"])
    electrification_efficiency_df["electricity"] = ones(N_YEARS)
    electrification_efficiency_df["low_T_heat"] = ones(N_YEARS) 
    electrification_efficiency_df["high_T_heat"] = ones(N_YEARS)
    electrification_efficiency_df["transport"] = ones(N_YEARS)

def absolute_re_penetration_fun(x , L , x1, x99):
    original_absolute_renewable_penetration_array = s_curve_fun_vec(x = x, L = L, x1 = x1, x99 = x99) #(linspace(0,1,N_YEARS) *1) * 0.99
    
    #Calculate new installations by differentiating total installations fit to S-curve
    new_installations_list = [original_absolute_renewable_penetration_array[0]]
    for counter1 in range(N_SOLAR_LIFE-1):
        new_installations_list += [original_absolute_renewable_penetration_array[counter1 + 1] - original_absolute_renewable_penetration_array[counter1]]
    for counter1 in range(N_YEARS-N_SOLAR_LIFE):
        new_installations_list += [original_absolute_renewable_penetration_array[counter1 + N_SOLAR_LIFE] - original_absolute_renewable_penetration_array[counter1 + N_SOLAR_LIFE - 1] + new_installations_list[counter1]]
    ########## Build DataFrame with all PV installations and efficiencies #########
    
    installed_area_array = array(new_installations_list) / CURRENT_PV_EFFICIENCY
    year_installed_array = arange(N_YEARS)
    initial_efficiency_array =  arange(N_YEARS) * PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE + CURRENT_PV_EFFICIENCY
    initial_efficiency_array = array((initial_efficiency_array < FINAL_PV_EFFICIENCY),dtype = float) * initial_efficiency_array + array(~(initial_efficiency_array < FINAL_PV_EFFICIENCY),dtype = float) * FINAL_PV_EFFICIENCY
    installed_pv_df = pd.DataFrame({
            "year_installed": year_installed_array,
            "installed_area": installed_area_array,
            "initial_efficiency": initial_efficiency_array
            })
    
    ########### Calculate Effective Cumulative PV Installation ##############
    
    effective_cumulative_pv_array = zeros(N_YEARS)
    for counter1 in range(size(effective_cumulative_pv_array)):
        year = counter1
        effective_installations = (1 - (year - installed_pv_df["year_installed"]) * PV_DEGREDATION_RATE) * installed_pv_df["initial_efficiency"] * installed_pv_df["installed_area"]
        if counter1<N_SOLAR_LIFE:
            effective_cumulative_pv_array[counter1] = sum(effective_installations[:counter1+1])
        else:
            effective_cumulative_pv_array[counter1] = sum(effective_installations[counter1+1 - N_SOLAR_LIFE:counter1+1])
    
    return effective_cumulative_pv_array
        

def re_penetration_simulation(n_years, absolute_re_penetration_l, absolute_re_penetration_x1, absolute_re_penetration_x99, 
                              fractional_re_penetration_l, fractional_re_penetration_x1, fractional_re_penetration_x99):
    #re_penetration_array = zeros(N_YEARS)
    #absolute_re_penetration_array = s_curve_fun_vec(x = arange(N_YEARS), L = ABSOLUTE_RE_PENETRATION_L, x1 = ABSOLUTE_RE_PENETRATION_X1, x99 = ABSOLUTE_RE_PENETRATION_X99) #(linspace(0,1,N_YEARS) *1) * 0.99
    absolute_re_penetration_array = absolute_re_penetration_fun(x = arange(n_years), L = absolute_re_penetration_l, x1 = absolute_re_penetration_x1, x99 = absolute_re_penetration_x99)
    fractional_re_penetration_array = s_curve_fun_vec(x = arange(n_years), L = fractional_re_penetration_l, x1 = fractional_re_penetration_x1, x99 = fractional_re_penetration_x99)
    return [absolute_re_penetration_array, fractional_re_penetration_array]

def primary_to_secondary_demand(primary_demand_df, electrification_df):
    new_electricity_demand_array = sum(primary_demand_df * electrification_df, axis = 1)
    secondary_demand_df = primary_demand_df * (1-electrification_df)
    secondary_demand_df["electricity"] = new_electricity_demand_array
    return secondary_demand_df

def secondary_demand_and_re_penetration_to_emissions(secondary_demand_df, re_penetration_array, co2_intensity_df):
    electricity_co2_array = re_penetration_array * solar_co2_array + (1-re_penetration_array) * fossil_co2_array
    co2_intensity_df["electricity"] = electricity_co2_array
    emissions_df = secondary_demand_df * co2_intensity_df
    return emissions_df

def conversion_matrix_sim_fun(eff_matrix, conv_factor_matrix):
    return divide(conv_factor_matrix, eff_matrix)




def sd_ss_conversion_matrix_builder(sd_ss_conversion_fraction_matrix, s_electricity_demand, absolute_re_penetration, fractional_re_penetration):
    test_matrix = sd_ss_conversion_fraction_matrix.copy()
    test_matrix[4,3] = absolute_re_penetration / s_electricity_demand
    test_matrix[4,3] = test_matrix[4,3] + (1 - test_matrix[4,3]) * fractional_re_penetration
    test_matrix[0,3] = 1 - test_matrix[1,3] - test_matrix[2,3] - test_matrix[3,3] - test_matrix[4,3]
    # test_matrix[3,3] = 0.01
    # test_matrix[2,3] = 0
    # test_matrix[1,3] = 0
    # test_matrix[0,3] = 0
    # test_matrix[2,3] = 1 - test_matrix[1,3] - test_matrix[0,3] - test_matrix[3,3] - test_matrix[4,3]
    # test_matrix[1,3] = 1 - test_matrix[0,3] - test_matrix[2,3] - test_matrix[3,3] - test_matrix[4,3]
    return test_matrix


def annual_emissions_simulation(pd_sd_efficiency_matrix, pd_sd_conversion_fraction_matrix,
                                sd_ss_efficiency_matrix, old_sd_ss_conversion_fraction_matrix,
                                ss_ps_efficiency_matrix, ss_ps_conversion_fraction_matrix,
                                pd_vector, ps_co2_intensity_df, 
                                absolute_re_penetration, fractional_re_penetration):
    pd_sd_matrix = divide(pd_sd_conversion_fraction_matrix, pd_sd_efficiency_matrix)
    sd_vector = pd_sd_matrix * pd_vector
    ### calculate sd_ss_connversion_fraction_matrix
    sd_ss_conversion_fraction_matrix = sd_ss_conversion_matrix_builder(old_sd_ss_conversion_fraction_matrix, sd_vector[3], absolute_re_penetration, fractional_re_penetration)
    sd_ss_matrix = divide(sd_ss_conversion_fraction_matrix, sd_ss_efficiency_matrix)
    ss_ps_matrix = divide(ss_ps_conversion_fraction_matrix, ss_ps_efficiency_matrix)
    ##### Alternate calculations for annual emissions for each primary demand #######
    emissions_factors_vector = matrix(ps_co2_intensity_df["carbon_intensity"]) * ss_ps_matrix * sd_ss_matrix * pd_sd_matrix
    annual_emissions_array = zeros(size(pd_vector))
    for counter1 in range(size(pd_vector)):
        annual_emissions_array[counter1] = emissions_factors_vector[0,counter1] * pd_vector[counter1]
    annual_emissions = sum(annual_emissions_array)
    return annual_emissions_array, sd_ss_conversion_fraction_matrix[4,3] # annual emissions for each pd, overall fractional re_pen


########### Load/Set Parameters #############
    
N_YEARS = 35 #Number of years to simulate


####### list of names of energy vectors ############
ps_vector_names_array = array(["natural_gas","oil","coal","waste","solar"])
ss_vector_names_array = array(["natural_gas","oil","coal","waste","solar"])
sd_vector_names_array = array(["natural_gas","oil","coal","electricity"])
pd_vector_names_array = array(["electricity","low_T_heat","high_T_heat","transport"]) # units of [TWh, TWh, TWh, Tm (car)]



###### Set Efficiency, conversion fraction and flow constraint Matrices ########
pd_sd_efficiency_matrix = matrix([[0.51,1,1,0.0001],[0.36,1,1,1.4],[0.36,1,1,0.0001],[1,4,1,5]]) # matrix of conversion efficiency from ith row secondary demand to jth column primary demand.

pd_sd_conversion_fraction_matrix = matrix([[0,0.5,0.5,0],[0,0.25,0.25,1],[0,0.25,0.25,0],[1,0,0,0]]) # matrix of fraction of demand converted from ith row secondary demand to jth column primary demand.

#pd_sd_matrix = divide(pd_sd_conversion_fraction_matrix, pd_sd_efficiency_matrix)

sd_ss_efficiency_matrix = matrix([[1,0.0001,0.0001,0.51],[0.0001,1,0.0001,0.36],[0.0001,0.0001,1,0.36],[0.0001,0.0001,0.0001,0.36],[0.0001,0.0001,0.0001,1]]) # matrix of conversion efficiency from ith row secondary supply to jth column secondary demand.

sd_ss_conversion_fraction_matrix = matrix([[1,0,0,0.98],[0,1,0,0],[0,0,1,0],[0,0,0,0.01],[0,0,0,0.01]]) # matrix of fraction of supply converted from ith row secondary supply to jth column secondary demand.
#sd_ss_conversion_fraction_matrix = matrix([[1,0,0,0],[0,1,0,0],[0,0,1,1],[0,0,0,0],[0,0,0,0]]) # matrix of fraction of supply converted from ith row secondary supply to jth column secondary demand.


#sd_ss_matrix = divide(sd_ss_conversion_fraction_matrix, sd_ss_efficiency_matrix)

ss_ps_efficiency_matrix = matrix([[1,0.0001,0.0001,0.0001,0.0001],[0.0001,1,0.0001,0.0001,0.0001],[0.0001,0.0001,1,0.0001,0.0001],[0.0001,0.0001,0.0001,1,0.0001],[0.0001,0.0001,0.0001,0.0001,1]]) # matrix of conversion efficiency from ith row primary supply to jth column secondary supply.

ss_ps_conversion_fraction_matrix = matrix([[1,0,0,0,0],[0,1,0,0,0],[0,0,1,0,0],[0,0,0,1,0],[0,0,0,0,1]]) # matrix of fraction of supply converted from ith row primary supply to jth column secondary supply.

#ss_ps_matrix = divide(ss_ps_conversion_fraction_matrix, ss_ps_efficiency_matrix)

### Set Electricity Emissions From Solar##########
solar_co2_array = zeros(N_YEARS)# tonne of CO2 per TWh

### Set Electricity Emissions From Fossil Fuels##########
fossil_co2_array = ones(N_YEARS)# tonne of CO2 per TWh

##### Set Emissions Intensity For All Energy Types ########
co2_intensity_df = pd.DataFrame(data = ones((N_YEARS,4)), columns = ["electricity", "low_T_heat", "high_T_heat", "transport"])# Mega tonne of CO2 per TWh

###### Primary Supply CO2 Intensity ###########
ps_co2_intensity_data = array(zeros((2,size(ps_vector_names_array))),dtype = object)
ps_co2_intensity_data[0] = ps_vector_names_array
#ps_co2_intensity_data[1] = array([200.9117799,261.3100602,333.6850387,356.6882201,27.71470335])/1000
ps_co2_intensity_data[1] = array([200.9117799,261.3100602,333.6850387,356.6882201,0])/1000# Set CO2 intensity of solar to zero
ps_co2_intensity_df = pd.DataFrame(data = ps_co2_intensity_data.T, columns = ["name","carbon_intensity"])# Mega tonne of CO2 per TWh



########### Main Simulation #############

###### Primary Demand ########
primary_demand_df = primary_demand_prediction(primary_demand_original_df)



######## Plotting Data List #########
plot1_data_x_list = []
plot1_data_y1_list = []
plot1_data_y2_list = []
plot1_data_y3_list = []
plot1_data_y4_list = []

plot2_data_x_list = []
plot2_data_y1_list = []
plot2_data_y2_list = []

plot3_data_x_list = []
plot3_data_y_list = []

total_emissions_list = []

n_years = N_YEARS

for counter1 in range(4):
    LOW_T_HEAT_ELECTRIFICATION_L = LOW_T_HEAT_ELECTRIFICATION_L_array[counter1]
    LOW_T_HEAT_ELECTRIFICATION_X1 = LOW_T_HEAT_ELECTRIFICATION_X1_array[counter1]
    LOW_T_HEAT_ELECTRIFICATION_X99 = LOW_T_HEAT_ELECTRIFICATION_X99_array[counter1]
    
    HIGH_T_HEAT_ELECTRIFICATION_L = HIGH_T_HEAT_ELECTRIFICATION_L_array[counter1]
    HIGH_T_HEAT_ELECTRIFICATION_X1 = HIGH_T_HEAT_ELECTRIFICATION_X1_array[counter1]
    HIGH_T_HEAT_ELECTRIFICATION_X99 = HIGH_T_HEAT_ELECTRIFICATION_X99_array[counter1]
    
    TRANSPORT_ELECTRIFICATION_L = TRANSPORT_ELECTRIFICATION_L_array[counter1]
    TRANSPORT_ELECTRIFICATION_X1 = TRANSPORT_ELECTRIFICATION_X1_array[counter1]

    absolute_re_penetration_l = ABSOLUTE_RE_PENETRATION_L_array[counter1]
    absolute_re_penetration_x1 = ABSOLUTE_RE_PENETRATION_X1_array[counter1]
    absolute_re_penetration_x99 = ABSOLUTE_RE_PENETRATION_X99_array[counter1]
    
    fractional_re_penetration_l = FRACTIONAL_RE_PENETRATION_L_array[counter1]
    fractional_re_penetration_x1 = FRACTIONAL_RE_PENETRATION_X1_array[counter1]
    fractional_re_penetration_x99 = FRACTIONAL_RE_PENETRATION_X99_array[counter1]
    
    ##### RE Penetration #######
    #absolute_re_penetration_array, fractional_re_penetration_array = re_penetration_simulation()
    absolute_re_penetration_array, fractional_re_penetration_array = re_penetration_simulation(n_years, 
        absolute_re_penetration_l, absolute_re_penetration_x1, absolute_re_penetration_x99, 
        fractional_re_penetration_l, fractional_re_penetration_x1, fractional_re_penetration_x99)
    overall_re_penetration_array = zeros_like(absolute_re_penetration_array) # overall fractional RE Pen
    
    ###### Electrification ########
    
    electrification_df = electrification_simulation()
    
    pd_sd_conversion_fraction_matrix_list = []
    for counter2 in range(electrification_df.shape[0]):
        test_matrix = pd_sd_conversion_fraction_matrix.copy()
        test_matrix[3] = matrix(electrification_df.iloc[counter2])
        elec_frac = (1 - electrification_df.iloc[counter2]) / (1 - electrification_df.iloc[0])
        for counter3 in range(size(elec_frac)):
            if (~(elec_frac[counter3] >= 0)):
                elec_frac[counter3] = 0
        for counter3 in range(3):
            test_matrix[counter3] = matrix(elec_frac * array(test_matrix)[counter3])
        pd_sd_conversion_fraction_matrix_list += [test_matrix]
    
    
    ##### Simulation ##########
    
    annual_emissions_array = zeros(primary_demand_df.shape)
    
    for counter2 in range(shape(primary_demand_df)[0]):
        annual_emissions_array[counter2], overall_re_penetration_array[counter2] = annual_emissions_simulation(
            pd_sd_efficiency_matrix, pd_sd_conversion_fraction_matrix_list[counter2],
            sd_ss_efficiency_matrix, sd_ss_conversion_fraction_matrix,
            ss_ps_efficiency_matrix, ss_ps_conversion_fraction_matrix,
            matrix(primary_demand_df.iloc[counter2]).T, ps_co2_intensity_df,
            absolute_re_penetration_array[counter2], fractional_re_penetration_array[counter2])
    
    print("Total Emissions = ", sum(annual_emissions_array), "(Million tonnes of CO2)")
    print("Final Annual Emissions = ",sum(annual_emissions_array[-1]),"(Million tonnes of CO2)")
    
    ######## Plotting Data #######
    
    plot1_data_x_list += [(arange(0,shape(annual_emissions_array)[0]))[4:] + 2016]
    plot1_data_y1_list += [(electrification_df[electrification_df.columns[1]])[4:]*100]
    plot1_data_y2_list += [(electrification_df[electrification_df.columns[2]])[4:]*100]
    plot1_data_y3_list += [(electrification_df[electrification_df.columns[3]])[4:]*100]
    #plot1_data_y4_list += [absolute_re_penetration_array/ABSOLUTE_RE_PENETRATION_L*100]
    plot1_data_y4_list += [overall_re_penetration_array[4:]*100]
    
    plot2_data_x_list += [(arange(0,shape(annual_emissions_array)[0]))[4:] + 2016]
    plot2_data_y1_list += [(annual_emissions_array)[4:].T]
    plot2_data_y2_list += [sum(annual_emissions_array[4:], axis = 1)]
    
    plot3_data_x_list += [(arange(0,shape(annual_emissions_array)[0]))[4:] + 2016]
    plot3_data_y_list += [cumsum(sum(annual_emissions_array[4:], axis = 1))]
    
    total_emissions_list += [sum(annual_emissions_array[4:])]




######## Plot Emissions ############

N_YEARS_2 = N_YEARS - 4

def plot_pathways(ax, data_number, case_name, figure_name):
    ax.set_title( case_name + ' Pathway', fontsize = 20)
    plot_data_x = array(plot1_data_x_list[data_number])
    plot_data_y1 = array(plot1_data_y1_list[data_number])
    plot_data_y2 = array(plot1_data_y2_list[data_number])
    plot_data_y3 = array(plot1_data_y3_list[data_number])
    plot_data_y4 = array(plot1_data_y4_list[data_number])
    if data_number == 0:
        ax.plot(plot_data_x,plot_data_y1,"y^-", markevery = list(arange(0,N_YEARS_2,4)), label = "Low T Heat")
        ax.plot(plot_data_x,plot_data_y2, "g.-", markevery = list(arange(1,N_YEARS_2,4)), label = "High T Heat")
        ax.plot(plot_data_x,plot_data_y3, "rx-", markevery = list(arange(2,N_YEARS_2,4)), label = "Transport")
        ax.plot(plot_data_x, plot_data_y4, "k+-", markevery = list(arange(3,N_YEARS_2,4)), label = "RE Pen")
    else:
        ax.plot(plot_data_x,plot_data_y1,"y^-", markevery = list(arange(0,N_YEARS_2,4)))#, label = "Low T Heat")
        ax.plot(plot_data_x,plot_data_y2, "g.-", markevery = list(arange(1,N_YEARS_2,4)))#, label = "High T Heat")
        ax.plot(plot_data_x,plot_data_y3, "rx-", markevery = list(arange(2,N_YEARS_2,4)))#, label = "Transport")
        ax.plot(plot_data_x, plot_data_y4, "k+-", markevery = list(arange(3,N_YEARS_2,4)))#, label = "RE Pen")
    ax.set_ylim(-5,105)
    ax.text(2018,112, figure_name, fontsize = 20, bbox = bbox_style)
    ax.set_yticks(ticks = arange(0,101,20))
    ax.grid(1)


def plot_emissions(ax, data_number, case_name, figure_name):
    ax.set_title( case_name + ' Pathway', fontsize = 20)
    plot_data_x = plot2_data_x_list[data_number]
    plot_data_y1 = plot2_data_y1_list[data_number]
    plot_data_y2 = plot2_data_y2_list[data_number]
    if data_number == 0:
        plot_labels = primary_demand_df.columns
        ax.stackplot(plot_data_x, plot_data_y1, colors = stackplot_colors, labels=plot_labels)
    else:
        ax.stackplot(plot_data_x, plot_data_y1, colors = stackplot_colors)#, labels=plot11_labels)
    ax.text(2020,82, "Total Emissions: " + str(round(total_emissions_list[data_number]/1000, 2)) + " GTCO$_2$", fontsize = 16, color = "k", bbox=dict(facecolor='white', alpha=0.5))
    ax.text(2018,106, figure_name, fontsize = 20, bbox = bbox_style)
    ax.set_ylim(0,100)
    ax.set_yticks(ticks = arange(0,101,20))
    ax.grid(1)


fig, ((ax1, ax2), (ax3, ax4), (ax5, ax6), (ax7, ax8)) = plt.subplots(nrows=4, ncols=2, figsize = (9,12))
ax_list = [[ax1, ax2], [ax3, ax4], [ax5, ax6], [ax7, ax8]]

#case_name_list = ["BAU(b)", "Case 1(b)", "Case 2(b)", "Case 3(b)"]
#figure_name_list = ["(a)","(b)","(c)","(d)","(e)","(f)","(g)","(h)"]

plt.rc("xtick", labelsize = 20)
plt.rc("ytick", labelsize = 20)
plt.rc("legend", fontsize = 16)

bbox_style = dict(boxstyle='round', facecolor='white', alpha=0.5)
stackplot_colors = ("c","y","g","r")

for counter1 in range(4):
    plot_pathways(ax_list[counter1][0], counter1, case_name_list[counter1], figure_name_list[counter1 * 2])
    plot_emissions(ax_list[counter1][1], counter1, case_name_list[counter1],figure_name_list[counter1 * 2 + 1])
    



fig.text(0.5,-0.08," ", fontsize = 20)#added to make space at the bottom of the figure
fig.text(0.5,-0.01,"Year", fontsize = 20)
fig.text(-0.01,0.36,"Electrification/ RE Penetration (%)", rotation = 90, fontsize = 20)
fig.legend(loc = 8, ncol = 4)
fig.suptitle("Singapore Decarbonization Pathway and Emissions \n (" + case_title + ")", y = 1.05, fontsize= 22)

fig.tight_layout()
plt.show()




"""
plt.rc("xtick", labelsize = 16)
plt.rc("ytick", labelsize = 16)
plt.rc("legend", fontsize = 12)


fig = plt.figure(figsize=(9,12))


ax1 = plt.subplot2grid((4,2),(0,0),colspan=1, rowspan = 1)
ax2 = plt.subplot2grid((4,2),(0,1),colspan=1, rowspan = 1)
#ax3 = ax2.twinx()
ax4 = plt.subplot2grid((4,2),(1,0),colspan=1, rowspan = 1)
ax5 = plt.subplot2grid((4,2),(1,1),colspan=1, rowspan = 1)
#ax6 = ax5.twinx()
ax7 = plt.subplot2grid((4,2),(2,0),colspan=1, rowspan = 1)
ax8 = plt.subplot2grid((4,2),(2,1),colspan=1, rowspan = 1)
#ax9 = ax8.twinx()
ax10 = plt.subplot2grid((4,2),(3,0),colspan=1, rowspan = 1)
ax11 = plt.subplot2grid((4,2),(3,1),colspan=1, rowspan = 1)
#ax12 = ax11.twinx()

bbox_style = dict(boxstyle='round', facecolor='white', alpha=0.5)
stackplot_colors = ("c","y","g","r")


ax1.set_title('BAU Pathway', fontsize = 16)
plot1_data_x = array(plot1_data_x_list[0])
plot1_data_y1 = array(plot1_data_y1_list[0])
plot1_data_y2 = array(plot1_data_y2_list[0])
plot1_data_y3 = array(plot1_data_y3_list[0])
plot1_data_y4 = array(plot1_data_y4_list[0])
ax1.plot(plot1_data_x,plot1_data_y1,"y^-", markevery = list(arange(0,N_YEARS_2,4)), label = "Low T Heat")
ax1.plot(plot1_data_x,plot1_data_y2, "g.-", markevery = list(arange(1,N_YEARS_2,4)), label = "High T Heat")
ax1.plot(plot1_data_x,plot1_data_y3, "rx-", markevery = list(arange(2,N_YEARS_2,4)), label = "Transport")
ax1.plot(plot1_data_x, plot1_data_y4, "k+-", markevery = list(arange(3,N_YEARS_2,4)), label = "RE Pen")
ax1.set_ylabel("Electrification/ RE Penetration (%)", fontsize = 16, y = -1.5)
ax1.set_ylim(-5,105)
ax1.set_yticks(ticks = arange(0,101,20))
ax1.set_xticks(ticks = arange(2020,2051, 10))
ax1.grid(1)

ax1.text(2018,112,"(a)", fontsize = 16, bbox = bbox_style)

#ax1.legend(loc='center right')

ax2.set_title('BAU Emissions', fontsize = 16)
plot2_data_x = plot2_data_x_list[0]
plot2_data_y1 = plot2_data_y1_list[0]
plot2_data_y2 = plot2_data_y2_list[0]

plot2_labels = primary_demand_df.columns
ax2.stackplot(plot2_data_x, plot2_data_y1, labels=plot2_labels, colors = stackplot_colors)
ax2.set_ylabel("Annual GHG Emissions (MTCO2)", fontsize = 16, y = -1.5)
ax2.text(2020,82, "Total Emissions: " + str(round(total_emissions_list[0]/1000, 2)) + " GTCO$_2$", fontsize = 14, color = "k", bbox=dict(facecolor='white', alpha=0.5))
ax2.text(2018,105,"(b)", fontsize = 16, bbox = bbox_style)
ax2.set_ylim(0,100)
ax2.set_yticks(ticks = arange(0,101,20))
ax2.grid(1)
#ax2.legend(loc='lower right')


ax4.set_title('Case 1 Pathway', fontsize = 16)
plot4_data_x = array(plot1_data_x_list[1])
plot4_data_y1 = array(plot1_data_y1_list[1])
plot4_data_y2 = array(plot1_data_y2_list[1])
plot4_data_y3 = array(plot1_data_y3_list[1])
plot4_data_y4 = array(plot1_data_y4_list[1])
ax4.plot(plot4_data_x,plot4_data_y1,"y^-", markevery = list(arange(0,N_YEARS_2,4)))#, label = "Low T Heat")
ax4.plot(plot4_data_x,plot4_data_y2, "g.-", markevery = list(arange(1,N_YEARS_2,4)))#, label = "High T Heat")
ax4.plot(plot4_data_x,plot4_data_y3, "rx-", markevery = list(arange(2,N_YEARS_2,4)))#, label = "Transport")
ax4.plot(plot4_data_x, plot4_data_y4, "k+-", markevery = list(arange(3,N_YEARS_2,4)))#, label = "RE Pen")
ax4.set_ylim(-5,106)
ax4.text(2018,112,"(c)", fontsize = 16, bbox = bbox_style)
ax4.set_yticks(ticks = arange(0,101,20))
ax4.grid(1)

#ax4.legend(loc='center right')

ax5.set_title('Case 1 Emissions', fontsize = 16)
plot5_data_x = plot2_data_x_list[1]
plot5_data_y1 = plot2_data_y1_list[1]
plot5_data_y2 = plot2_data_y2_list[1]

plot5_labels = primary_demand_df.columns
ax5.stackplot(plot5_data_x, plot5_data_y1, colors = stackplot_colors)#, labels=plot5_labels)
ax5.text(2020,82, "Total Emissions: " + str(round(total_emissions_list[1]/1000, 2)) + " GTCO$_2$", fontsize = 14, color = "k", bbox=dict(facecolor='white', alpha=0.5))
ax5.text(2018,106,"(d)", fontsize = 16, bbox = bbox_style)
ax5.set_ylim(0,100)
ax5.set_yticks(ticks = arange(0,101,20))
ax5.grid(1)
#ax5.set_ylabel("Annual GHG Emissions (MTCO2)", fontsize = 16, y = -0.8)
#ax5.legend(loc='lower right')


ax7.set_title('Case 2 Pathway', fontsize = 16)
plot7_data_x = array(plot1_data_x_list[2])
plot7_data_y1 = array(plot1_data_y1_list[2])
plot7_data_y2 = array(plot1_data_y2_list[2])
plot7_data_y3 = array(plot1_data_y3_list[2])
plot7_data_y4 = array(plot1_data_y4_list[2])
ax7.plot(plot7_data_x,plot7_data_y1,"y^-", markevery = list(arange(0,N_YEARS_2,4)))#, label = "Low T Heat")
ax7.plot(plot7_data_x,plot7_data_y2, "g.-", markevery = list(arange(1,N_YEARS_2,4)))#, label = "High T Heat")
ax7.plot(plot7_data_x,plot7_data_y3, "rx-", markevery = list(arange(2,N_YEARS_2,4)))#, label = "Transport")
ax7.plot(plot7_data_x, plot7_data_y4, "k+-", markevery = list(arange(3,N_YEARS_2,4)))#, label = "RE Pen")
#ax7.set_ylabel("Electrification/ RE Penetration (%)")
ax7.set_ylim(-5,105)
ax7.text(2018,112,"(e)", fontsize = 16, bbox = bbox_style)
ax7.set_yticks(ticks = arange(0,101,20))
ax7.grid(1)
#ax7.legend(loc='center right')

ax8.set_title('Case 2 Emissions', fontsize = 16)
plot8_data_x = plot2_data_x_list[2]
plot8_data_y1 = plot2_data_y1_list[2]
plot8_data_y2 = plot2_data_y2_list[2]

plot8_labels = primary_demand_df.columns
ax8.stackplot(plot8_data_x, plot8_data_y1, colors = stackplot_colors)#, labels=plot8_labels)
ax8.text(2020,82, "Total Emissions: " + str(round(total_emissions_list[2]/1000, 2)) + " GTCO$_2$", fontsize = 14, color = "k", bbox=dict(facecolor='white', alpha=0.5))
ax8.text(2018,106,"(f)", fontsize = 16, bbox = bbox_style)
ax8.set_ylim(0,100)
ax8.set_yticks(ticks = arange(0,101,20))
ax8.grid(1)
#ax8.set_ylabel("Annual GHG Emissions (MTCO2)")
#ax8.legend(loc='lower right')


ax10.set_title('Case 3 Pathway', fontsize = 16)
plot10_data_x = array(plot1_data_x_list[3])
plot10_data_y1 = array(plot1_data_y1_list[3])
plot10_data_y2 = array(plot1_data_y2_list[3])
plot10_data_y3 = array(plot1_data_y3_list[3])
plot10_data_y4 = array(plot1_data_y4_list[3])
ax10.plot(plot10_data_x,plot10_data_y1,"y^-", markevery = list(arange(0,N_YEARS_2,4)))#, label = "Low T Heat")
ax10.plot(plot10_data_x,plot10_data_y2, "g.-", markevery = list(arange(1,N_YEARS_2,4)))#, label = "High T Heat")
ax10.plot(plot10_data_x,plot10_data_y3, "rx-", markevery = list(arange(2,N_YEARS_2,4)))#, label = "Transport")
ax10.plot(plot10_data_x, plot10_data_y4, "k+-", markevery = list(arange(3,N_YEARS_2,4)))#, label = "RE Pen")
#ax10.set_ylabel("Electrification/ RE Penetration (%)")
ax10.set_ylim(-5,105)
ax10.text(2018,112,"(g)", fontsize = 16, bbox = bbox_style)
ax10.set_yticks(ticks = arange(0,101,20))
ax10.grid(1)
#ax10.legend(loc='center right')

ax11.set_title('Case 3 Emissions', fontsize = 16)
plot11_data_x = plot2_data_x_list[3]
plot11_data_y1 = plot2_data_y1_list[3]
plot11_data_y2 = plot2_data_y2_list[3]

plot11_labels = primary_demand_df.columns
ax11.stackplot(plot11_data_x, plot11_data_y1, colors = stackplot_colors)#, labels=plot11_labels)
ax11.text(2020,82, "Total Emissions: " + str(round(total_emissions_list[3]/1000, 2)) + " GTCO$_2$", fontsize = 14, color = "k", bbox=dict(facecolor='white', alpha=0.5))
ax11.text(2018,106,"(h)", fontsize = 16, bbox = bbox_style)
ax11.set_ylim(0,100)
ax11.set_yticks(ticks = arange(0,101,20))
ax11.grid(1)
#ax11.set_ylabel("Annual GHG Emissions (MTCO2)")
#ax11.legend(loc='lower right')

fig.text(0.5,0.15,"Year", fontsize = 16)
fig.legend(loc = (0.15,0.0), ncol = 4)
fig.suptitle("Singapore Decarbonization Pathway and Emissions \n (With Perovskite; Without External Solar)", y = 1.05, fontsize= 20)

fig.tight_layout()
plt.show()

"""



###### Plot Primary Demand ########
plot12_data_x = (arange(N_YEARS) + 2016)[4:]
plot12_data_y = array(primary_demand_df)[4:]
plot_12_labels = primary_demand_df.columns
stackplot_colors = ("c","y","g","r")
plt.stackplot(plot12_data_x, plot12_data_y.T, labels=plot_12_labels, colors = stackplot_colors)
plt.xlabel("Year", fontsize = 16)
plt.ylabel("Annual Primary Demand (TWh or Tm)", fontsize = 16)
plt.legend(loc = "upper center", fontsize = 16)
plt.title("Singapore Annual Primary Energy Demand", fontsize = 16)
plt.show()



########## Run Simulation for Emissions Reductions vs Electrification Start Times #########

ELECTRIFICATION_TIME_ARRAY = array([10] * 3) # Time to electrify [Low T heat, High T heat, Transport]

LOW_T_HEAT_ELECTRIFICATION_L_array = array([0,1,0,0])

HIGH_T_HEAT_ELECTRIFICATION_L_array = array([0,0,1,0])

TRANSPORT_ELECTRIFICATION_L_array = array([0,0,0,1])

n_years = N_YEARS

plot_data_x_array = zeros((4, N_YEARS))
plot_data_y_array = zeros((4, N_YEARS, primary_demand_df.shape[1]))



for counter1 in range(4):
    LOW_T_HEAT_ELECTRIFICATION_L = LOW_T_HEAT_ELECTRIFICATION_L_array[counter1]
    
    HIGH_T_HEAT_ELECTRIFICATION_L = HIGH_T_HEAT_ELECTRIFICATION_L_array[counter1]
    
    TRANSPORT_ELECTRIFICATION_L = TRANSPORT_ELECTRIFICATION_L_array[counter1]
    
    absolute_re_penetration_l = ABSOLUTE_RE_PENETRATION_L_array[counter1]
    absolute_re_penetration_x1 = ABSOLUTE_RE_PENETRATION_X1_array[counter1]
    absolute_re_penetration_x99 = ABSOLUTE_RE_PENETRATION_X99_array[counter1]
    
    fractional_re_penetration_l = FRACTIONAL_RE_PENETRATION_L_array[counter1]
    fractional_re_penetration_x1 = FRACTIONAL_RE_PENETRATION_X1_array[counter1]
    fractional_re_penetration_x99 = FRACTIONAL_RE_PENETRATION_X99_array[counter1]
    
    ##### RE Penetration #######
    absolute_re_penetration_array, fractional_re_penetration_array = re_penetration_simulation(n_years, 
        absolute_re_penetration_l, absolute_re_penetration_x1, absolute_re_penetration_x99, 
        fractional_re_penetration_l, fractional_re_penetration_x1, fractional_re_penetration_x99)
    overall_re_penetration_array = zeros_like(absolute_re_penetration_array) # overall fractional RE Pen
    
    
    annual_emissions_array = zeros(primary_demand_df.shape)
    
    
    for counter2 in range(N_YEARS):
        
        LOW_T_HEAT_ELECTRIFICATION_X1 = counter2
        LOW_T_HEAT_ELECTRIFICATION_X99 = LOW_T_HEAT_ELECTRIFICATION_X1 + ELECTRIFICATION_TIME_ARRAY[0]
        
        HIGH_T_HEAT_ELECTRIFICATION_X1 = counter2
        HIGH_T_HEAT_ELECTRIFICATION_X99 = HIGH_T_HEAT_ELECTRIFICATION_X1 + ELECTRIFICATION_TIME_ARRAY[1]
        
        TRANSPORT_ELECTRIFICATION_X1 = counter2
        TRANSPORT_ELECTRIFICATION_X99 = TRANSPORT_ELECTRIFICATION_X1 + ELECTRIFICATION_TIME_ARRAY[2]
    
    
            ###### Electrification ########
    
        electrification_df = electrification_simulation()
        
        pd_sd_conversion_fraction_matrix_list = []
        for counter3 in range(electrification_df.shape[0]):
            test_matrix = pd_sd_conversion_fraction_matrix.copy()
            test_matrix[3] = matrix(electrification_df.iloc[counter3])
            elec_frac = (1 - electrification_df.iloc[counter3]) / (1 - electrification_df.iloc[0])
            for counter4 in range(size(elec_frac)):
                if (~(elec_frac[counter4] >= 0)):
                    elec_frac[counter4] = 0
            for counter4 in range(3):
                test_matrix[counter4] = matrix(elec_frac * array(test_matrix)[counter4])
            pd_sd_conversion_fraction_matrix_list += [test_matrix]
    
    
    
        for counter3 in range(shape(primary_demand_df)[0]):
            annual_emissions_array[counter3], overall_re_penetration_array[counter3] = annual_emissions_simulation(
                pd_sd_efficiency_matrix, pd_sd_conversion_fraction_matrix_list[counter3],
                sd_ss_efficiency_matrix, sd_ss_conversion_fraction_matrix,
                ss_ps_efficiency_matrix, ss_ps_conversion_fraction_matrix,
                matrix(primary_demand_df.iloc[counter3]).T, ps_co2_intensity_df,
                absolute_re_penetration_array[counter3], fractional_re_penetration_array[counter3])
    
        plot_data_x_array[counter1, counter2] = counter2 + 2016
        plot_data_y_array[counter1, counter2] = sum(annual_emissions_array[4:], axis = 0)

plot_data_x_array = plot_data_x_array[:,4:]
plot_data_y_array = plot_data_y_array[:,4:,:]

plt.plot(plot_data_x_array[1],sum(plot_data_y_array, axis =-1)[0]-sum(plot_data_y_array, axis =-1)[1], label = "Low T Heat")
plt.plot(plot_data_x_array[2],sum(plot_data_y_array, axis =-1)[0]-sum(plot_data_y_array, axis =-1)[2], label = "High T Heat")
plt.plot(plot_data_x_array[3],sum(plot_data_y_array, axis =-1)[0]-sum(plot_data_y_array, axis =-1)[3], label = "Transport")
plt.title("Emissions Reductions vs Electrification Start Times \n (" + case_title + ")", fontsize = 16)
plt.legend(loc = legend_loc)
plt.xlabel("Electrification Start Time ($x_1$)", fontsize = 16)
plt.ylabel("Reduction in Cumulative \n Emissions (MT$CO_2$)", fontsize = 16)
plt.grid(1)
plt.show()

