######### Packages ############
#import csv
#import scipy as sp
import pandas as pd
from numpy import *
from numpy.random import *
#import time
#import scipy.stats
#import sys
import matplotlib.pyplot as plt
#import seaborn as sns
#import matplotlib


######### Files ############

folder = r"/Users/aniqahsan/Documents/Oxford_PhD/Emissions Simulation/Scripts/data"

pv_data_filename = folder + r"/singaopre_solar_installation_data.csv"

total_electricity_gen_filename = folder + r"/singapore_electricity_generation_capacity_data.csv"

######### Constants ############
N_PLOT = 165 #number of quarters to plot
N_SOLAR_LIFE = 30#lifespan of solar panels

COST_CAPACITY_M = -0.35759492882704225
COST_CAPACITY_C = 4.164362612795422

CAPACITY_TIME_M = 0.36511502393525835
CAPACITY_TIME_C = -723.2835617518782

CURRENT_PV_EFFICIENCY = 0.16
FINAL_PV_EFFICIENCY = 0.26
PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE = 0.01
PV_DEGREDATION_RATE = 0.005

CURRENT_PV_EFFICIENCY_1 = 0.16
FINAL_PV_EFFICIENCY_1 = 0.16
PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE_1 = 0.01
PV_DEGREDATION_RATE_1 = 0.005

CURRENT_PV_EFFICIENCY_2 = 0.16
FINAL_PV_EFFICIENCY_2 = 0.26
PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE_2 = 0.01
PV_DEGREDATION_RATE_2 = 0.005

MAX_AREA = 36.8#45 #km2
MAX_PV_CAPACITY = MAX_AREA * CURRENT_PV_EFFICIENCY * 1e3# 10.0 #MWp assuming CURRENT_PV_EFFICIENCY
PVOUT = 1300 #kWh/kWp


######## Functions #################
def scurve(x):
    st = m*x+c
    return 1/(1+exp(-st))

def capacity_time_fun(t):
    return exp(CAPACITY_TIME_M * t + CAPACITY_TIME_C)

def cost_capacity_fun(capacity):
    return exp(COST_CAPACITY_M * log(capacity) + COST_CAPACITY_C)


def simulate_installations(current_pv_efficiency, final_pv_efficiency, pv_efficiency_annual_linear_growth_rate,
                           pv_degredation_rate, base_pv_array):
    #Calculate new installations by differentiating total installations fit to S-curve
    new_installations_list = [base_pv_array[0]]
    for counter1 in range(N_SOLAR_LIFE*4-1):
        new_installations_list += [base_pv_array[counter1 + 1] - base_pv_array[counter1]]
    #new_installations_list += [base_pv_array[N_SOLAR_LIFE*4 + 1] - base_pv_array[N_SOLAR_LIFE*4] + new_installations_list[0]]
    for counter2 in range(N_PLOT-N_SOLAR_LIFE*4):
        new_installations_list += [base_pv_array[counter2 + N_SOLAR_LIFE*4] - base_pv_array[counter2 + N_SOLAR_LIFE*4 - 1] + new_installations_list[counter2]]
    
    ########## Build DataFrame with all PV installations and efficiencies #########
    
    installed_area_array = array(new_installations_list) / current_pv_efficiency
    year_installed_array = arange(N_PLOT) * 0.25 + 2009
    initial_efficiency_array = append(zeros(40), arange(N_PLOT-40) * 0.25 * pv_efficiency_annual_linear_growth_rate) + current_pv_efficiency
    initial_efficiency_array = array((initial_efficiency_array < final_pv_efficiency),dtype = float) * initial_efficiency_array + array(~(initial_efficiency_array < final_pv_efficiency),dtype = float) * final_pv_efficiency
    installed_pv_df = pd.DataFrame({
            "year_installed": year_installed_array,
            "installed_area": installed_area_array,
            "initial_efficiency": initial_efficiency_array
            })
    
    ########### Calculate Effective Cumulative PV Installation ##############
    
    effective_cumulative_pv_array = zeros(N_PLOT)
    for counter1 in range(size(effective_cumulative_pv_array)):
        year = counter1 * 0.25 + 2009
        effective_installations = (1 - (year - installed_pv_df["year_installed"]) * pv_degredation_rate) * installed_pv_df["initial_efficiency"] * installed_pv_df["installed_area"]
        if counter1<N_SOLAR_LIFE*4:
            effective_cumulative_pv_array[counter1] = sum(effective_installations[:counter1+1])
        else:
            effective_cumulative_pv_array[counter1] = sum(effective_installations[counter1+1 - N_SOLAR_LIFE*4:counter1+1])  
            #effective_cumulative_pv_array[counter1] = sum(effective_installations[:counter1+1])
    
    ########## Calculate Effective New PV Installations ########
    
    effective_new_pv_installations_array = effective_cumulative_pv_array[1:] - effective_cumulative_pv_array[:-1]
    
    return effective_new_pv_installations_array, effective_cumulative_pv_array

    


######## Data ##############
cumulative_pv_df = pd.read_csv(pv_data_filename).iloc[1:].reset_index(drop = True) # In MWp

new_pv_df = cumulative_pv_df.iloc[1:].reset_index(drop = True) - cumulative_pv_df.iloc[:-1].reset_index(drop = True)
new_pv_df["Time"] = cumulative_pv_df.iloc[:-1].reset_index(drop=True)["Time"] - cumulative_pv_df.iloc[:-1].reset_index(drop=True)["Time"].iloc[0]

cumulative_total_gen_df = pd.read_csv(total_electricity_gen_filename).reset_index(drop = True) # In MW

new_total_gen_df = (cumulative_total_gen_df.iloc[1:].reset_index(drop = True) - cumulative_total_gen_df.iloc[:-1].reset_index(drop = True))
new_total_gen_df = (new_total_gen_df > 0) * new_total_gen_df
new_total_gen_df["Time"] = cumulative_total_gen_df.iloc[:-1].reset_index(drop=True)["Time"] - cumulative_total_gen_df.iloc[:-1].reset_index(drop=True)["Time"].iloc[0]
new_total_gen_df[new_total_gen_df.columns[1]] = (new_total_gen_df[new_total_gen_df.columns[2]] +
                            new_total_gen_df[new_total_gen_df.columns[3]] +
                            new_total_gen_df[new_total_gen_df.columns[4]] +
                            new_total_gen_df[new_total_gen_df.columns[5]]
                            )



###### Calculate 
t = arange(N_PLOT)/4 + 2009


####### Fit total PV to s-curve #############
y21 = array(cumulative_pv_df["Total"])

y_total2 = []

for counter1 in arange(size(y21)):
    pos1 = int(floor(counter1 / 4))
    if (pos1 + 2 < shape(cumulative_total_gen_df)[0]):
        pos2 = pos1 + 1
    else:
        pos2 = pos1
    pos_diff = int(counter1 % 4)/4
    value1 = cumulative_total_gen_df["Total Registered Generation Capacity"].iloc[pos1+1]
    value2 = cumulative_total_gen_df["Total Registered Generation Capacity"].iloc[pos2+1]
    y_total2 += [(value1 + (value2 - value1) * pos_diff)]
    #y_total += [new_total_gen_df["Total Registered Generation Capacity"].iloc[pos] + y[pos]]

y_total2 = array(y_total2)
y_total2 = ones(size(y_total2)) * MAX_PV_CAPACITY# * 10000 # assuming a maximum capacity of 10GWp
#y_total = y_total * 0.2

y21 = y21/y_total2


inv2 = log(y21/(1-y21))

inv2_fit = array(list(inv2[4:24]) + list(inv2[36:]))
time2_fit = array(list(arange(4,24)) + list(arange(36,len(y21))))


[m,c], cov_matrix = polyfit(time2_fit,inv2_fit,1, cov = True)


y22 = scurve(arange(N_PLOT)) * MAX_PV_CAPACITY #MWp

x2 = arange(len(y21))
z2 = m * x2 + c

##### Calculate R**2 value ######
yi = inv2_fit
fi = m * time2_fit + c
r2_value  = 1 - sum((fi-yi)**2)/sum((yi - mean(yi))**2)

"""
#Calculate new installations by differentiating total installations fit to S-curve
new_installations_2_list = [y22[0]]
for counter1 in range(N_SOLAR_LIFE*4-1):
    new_installations_2_list += [y22[counter1 + 1] - y22[counter1]]
#new_installations_2_list += [y22[N_SOLAR_LIFE*4 + 1] - y22[N_SOLAR_LIFE*4] + new_installations_2_list[0]]
for counter2 in range(N_PLOT-N_SOLAR_LIFE*4):
    new_installations_2_list += [y22[counter2 + N_SOLAR_LIFE*4] - y22[counter2 + N_SOLAR_LIFE*4 - 1] + new_installations_2_list[counter2]]


########## Build DataFrame with all PV installations and efficiencies #########

installed_area_array = array(new_installations_2_list) / CURRENT_PV_EFFICIENCY
year_installed_array = arange(N_PLOT) * 0.25 + 2009
initial_efficiency_array = append(zeros(40), arange(N_PLOT-40) * 0.25 * PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE) + CURRENT_PV_EFFICIENCY
initial_efficiency_array = array((initial_efficiency_array < FINAL_PV_EFFICIENCY),dtype = float) * initial_efficiency_array + array(~(initial_efficiency_array < FINAL_PV_EFFICIENCY),dtype = float) * FINAL_PV_EFFICIENCY
installed_pv_df = pd.DataFrame({
        "year_installed": year_installed_array,
        "installed_area": installed_area_array,
        "initial_efficiency": initial_efficiency_array
        })

########### Calculate Effective Cumulative PV Installation ##############

effective_cumulative_pv_array = zeros(N_PLOT)
for counter1 in range(size(effective_cumulative_pv_array)):
    year = counter1 * 0.25 + 2009
    effective_installations = (1 - (year - installed_pv_df["year_installed"]) * PV_DEGREDATION_RATE) * installed_pv_df["initial_efficiency"] * installed_pv_df["installed_area"]
    if counter1<N_SOLAR_LIFE*4:
        effective_cumulative_pv_array[counter1] = sum(effective_installations[:counter1+1])
    else:
        effective_cumulative_pv_array[counter1] = sum(effective_installations[counter1+1 - N_SOLAR_LIFE*4:counter1+1])
    #effective_cumulative_pv_array[counter1] = sum(effective_installations[:counter1+1])


########## Calculate Effective New PV Installations ########

effective_new_pv_installations_array = effective_cumulative_pv_array[1:] - effective_cumulative_pv_array[:-1]

"""
########## Simulate new installations########


effective_new_pv_installations_array_1, effective_cumulative_pv_array_1 = simulate_installations(CURRENT_PV_EFFICIENCY_1, 
        FINAL_PV_EFFICIENCY_1, PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE_1, PV_DEGREDATION_RATE_1, y22)

effective_new_pv_installations_array_2, effective_cumulative_pv_array_2 = simulate_installations(CURRENT_PV_EFFICIENCY_2, 
        FINAL_PV_EFFICIENCY_2, PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE_2, PV_DEGREDATION_RATE_2, y22)



#plt.rcParams.update({'font.size': 14})

fig = plt.figure(figsize=(10,10))

ax1 = plt.subplot2grid((3,2),(0,0),colspan=1)
ax2 = plt.subplot2grid((3,2),(0,1),colspan=1)
ax3 = plt.subplot2grid((3,2),(1,0),colspan=2,rowspan=2)
ax4 = ax3.twinx()

bbox_style = dict(boxstyle='round', facecolor='white', alpha=0.5)

ax1.set_title('Training data fit',y=0.8, fontsize = 16)



plot1_y1 = array(list(y21[4:24]) + list(y21[36:])) * MAX_PV_CAPACITY# s-curve fit to total installations
plot1_x1 = array(list(t[4:24]) + list(t[36:size(y21)]))# s-curve fit to total installations

ax1.plot(plot1_x1,plot1_y1,"x",c='k',label='Used Data')

plot1_y2 = array(list(y21[24:36])) * MAX_PV_CAPACITY# s-curve fit to total installations. points that were not used
plot1_x2 = array(list(t[24:36]))# s-curve fit to total installations. points that were not used
ax1.plot(plot1_x2,plot1_y2,"x",c='r', label="Unused Data")



ax1.plot(t[4:size(y22)],y22[4:],label='S Curve') # s-curve fit to total installations
ax1.set_xlim(2009.5,2020.5)
ax1.set_ylim(0,0.06)
ax1.set_xticks(arange(2010,2021,2))
ax1.set_xticklabels(arange(2010,2021,2), fontsize = 16)
yticks1 = arange(0,450,50)
ylabels1 = around(arange(0,450,50)/MAX_PV_CAPACITY * MAX_AREA, 2)
ax1.set_yticks(yticks1)
ax1.set_yticklabels(ylabels1, fontsize = 16)
ax1.set_ylabel("Total Area Used for PV (km$^2$)", fontsize = 16)
ax1.grid()
ax1.text(2010.0,350,"(a)", fontsize = 16, bbox = bbox_style)


ax2.set_title('Fitting the S-Curve',y=0.8, fontsize = 16)



plot2_y1 = array(list(inv2[4:24]) + list(inv2[36:])) # s-curve fit to total installations
plot2_x1 = array(list(arange(4,24)) + list(arange(36,size(inv2)))) # s-curve fit to total installations

ax2.scatter(plot2_x1,plot2_y1,c='k',marker='x')

plot2_y2 = array(list(inv2[24:36]))# s-curve fit to total installations. points that were not used
plot2_x2 = array(list(arange(24,36)))# s-curve fit to total installations. points that were not used
ax2.scatter(plot2_x2,plot2_y2,c='r',marker='x')


ax2.plot(z2)# s-curve fit to total installations
ax2.set_xticks(arange(0,41,8)+4)
ax2.set_xticklabels(['2010','2012','2014','2016','2018','2020'], fontsize = 16)
yticklabels2 = array(2**arange(1,12)/2, dtype = int)
yticks2 = -log((yticklabels2/MAX_PV_CAPACITY)**-1 - 1)
yticklabels2 = around(yticklabels2/MAX_PV_CAPACITY * MAX_AREA, 2)
ax2.set_yticks(yticks2)
ax2.set_yticklabels(yticklabels2, fontsize = 16)
ax2.set_ylabel("Total Area Used for PV (km$^2$)", fontsize = 16)
ax2.set_ylim(-log((2/MAX_PV_CAPACITY)**-1 - 1),-log((512/MAX_PV_CAPACITY)**-1 - 1))
ax2.set_xlim(3,45)
ax2.grid()
ax2.text(5,-3.1,"(b)", fontsize = 16, bbox = bbox_style)
ax2.text(20,-7.1, "$R^2$ = " + str(round(r2_value, 4)), fontsize = 16)


ax3.plot(arange(N_PLOT-1),effective_new_pv_installations_array_1[:N_PLOT],color='C0')# s-curve fit to total installations
ax3.plot(arange(N_PLOT-1),effective_new_pv_installations_array_2[:N_PLOT], "--", color='C0')# s-curve fit to total installations




ax3.set_title('Projected Solar Capacity',y=0.8, fontsize = 16)
ax3.set_xticks(arange(4,180,20))
ax3.set_xticklabels(arange(2010,2055,5), fontsize = 16)
ax3.tick_params('y',labelcolor='C0')
ax3.set_yticks(arange(0.0,300,30))# s-curve fit to total installations
ax3.set_yticklabels(arange(0,100,10), fontsize = 16)
ax3.set_ylabel('Effective Change in Installed \n Solar Capacity Per Month (MW$_p$)',color='C0', fontsize = 16)
ax3.grid()
ax3.text(-1,250,"(c)", fontsize = 16, bbox = bbox_style)






plot4_y1 = array(list(y21[4:24]) + list(y21[36:]))* MAX_PV_CAPACITY# s-curve fit to total installations
plot4_x1 = array(list(arange(4,24)) + list(arange(36,size(y21))))# s-curve fit to total installations


ax4.scatter(plot4_x1,plot4_y1,c='k',marker='x')# s-curve fit to total installations


ax4.plot(arange(N_PLOT),effective_cumulative_pv_array_1[:N_PLOT],'g')# s-curve fit to total installations
ax4.plot(arange(N_PLOT),effective_cumulative_pv_array_2[:N_PLOT],'g--')# s-curve fit to total installations


ax4.set_yticks(arange(0.0,9000,1000)/10.4*10)
ax4.set_yticklabels(arange(0,11,1.25), fontsize = 16)
ax4.set_ylabel('Annual Solar Output (TWh)',color='g', fontsize = 16)
ax4.tick_params('y', colors='g')

fig.legend(loc = (0.167,0.90), ncol = 3, fontsize = 16)
fig.suptitle(t = "Estimated PV Land Use and Simulated PV Output in Singapore \n (With Single Junction or Perovskite PV)", x = 0.5, y = 1.07, fontsize = 16)

plt.tight_layout()


print("x1 = ", 2009 + -(log(0.01**-1 - 1) + c)/m/4)
print("x50 = ", 2009 + -(log(0.5**-1 - 1) + c)/m/4)
print("x99 = ", 2009 + -(log(0.99**-1 - 1) + c)/m/4)



######### Print Just one curve #############
"""
plt.figure(figsize=(4.5,3.5))

ax1 = plt.subplot2grid((1,1),(0,0),colspan=1)

ax1.set_title('Training data fit', fontsize = 16)

plot1_y1 = array(list(y21[4:24]) + list(y21[36:]))# s-curve fit to total installations
plot1_x1 = array(list(t[4:24]) + list(t[36:size(y21)]))# s-curve fit to total installations

ax1.plot(plot1_x1,plot1_y1,"x",c='k',label='Used Data')

plot1_y2 = array(list(y21[:4]) + list(y21[24:36]))# s-curve fit to total installations. points that were not used
plot1_x2 = array(list(t[:4]) + list(t[24:36]))# s-curve fit to total installations. points that were not used
ax1.plot(plot1_x2,plot1_y2,"x",c='r', label="Unused Data")

ax1.plot(t[:size(y22)],y22,label='S Curve') # s-curve fit to total installations
ax1.set_xlim(2009,2019)
ax1.set_ylim(0,0.03)
ax1.set_xticks(arange(2009,2020,2))
ax1.set_xticklabels(['2009','2011','2013','2015','2017','2019'], fontsize = 16)
ax1.set_yticks(arange(0,0.03,0.005))
ax1.set_yticklabels(arange(0,300,50), fontsize = 16)
ax1.set_ylabel("Total Solar Capacity (MWp)", fontsize = 16)
ax1.grid()
ax1.legend(ncol=1, fontsize = 16)

plt.tight_layout()



plt.figure(figsize=(9,5.5))

ax3 = plt.subplot2grid((1,1),(0,0),colspan=1)
ax4 = ax3.twinx()

ax3.plot(range(0,len(new_installations_2_list))[:124],new_installations_2_list[:124])# s-curve fit to total installations

ax3.set_title('Projected Solar Capacity', fontsize = 16)

ax3.set_xticks(range(4,140,20))
ax3.set_xticklabels(['2010','2015','2020','2025','2030','2035','2040'], fontsize = 16)
ax3.tick_params('y',labelcolor='C0')
ax3.set_yticks(arange(0.0,0.04,0.006))# s-curve fit to total installations
ax3.set_yticklabels(arange(0,121,20), fontsize = 16)
ax3.set_ylabel('New Solar Installations Per Month(MWp)',color='C0', fontsize = 16)
ax3.grid()


plot4_y1 = array(list(y21[4:24]) + list(y21[36:]))# s-curve fit to total installations
plot4_x1 = array(list(arange(4,24)) + list(arange(36,size(y21))))# s-curve fit to total installations

ax4.scatter(plot4_x1,plot4_y1,c='k',marker='x')# s-curve fit to total installations

ax4.plot(range(0,len(y22))[:124],y22[:124],'g')# s-curve fit to total installations
ax4.set_yticks(arange(0.0,1.2,0.2))
ax4.set_yticklabels(arange(0,11,2), fontsize = 16)
ax4.set_ylabel('Total Solar Capacity(GWp)',color='g', fontsize = 16)
ax4.tick_params('y', colors='g')

plt.tight_layout()
"""


####### Project the future cost of PV panels ###########
"""
pv_cost_array = cost_capacity_fun(capacity_time_fun(t))

installation_costs = vectorize(max)(pv_cost_array, ones(size(t)) * 0.1) * array(new_installations_2_list) * (1e10/3)


plt.plot(t, installation_costs/1e6) # price in Millions of USD per month
plt.ylabel("Capital Investments in Solar Panels per Month (Million USD)")


plt.plot(t,vectorize(max)(pv_cost_array, ones(size(t)) * 0.1))
plt.ylabel("Solar Panel Costs (USD/Wp)")
"""