This is a readme for Singapore_Sectoral_Reference_Emissions_Simulation

############ License #############
The MIT License (MIT)

Copyright (c) 2021 Aniq Ahsan

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


########## How to Use ###########
This repositiory includes two scripts, Singapore_cumulative_PV_s_curve_fitting_script.py and Emissions_Simulation_Script.py . The first script is used to fit historical data; the results of this fitting will be used as parameters for the second script.



############ Singapore_cumulative_PV_s_curve_fitting_script.py

Singapore_cumulative_PV_s_curve_fitting_script.py fits historical cumulative PV installations to an s-curve and then use that to estimate the annual solar output (including degradation). The script plots a figure and prints x1, x50 and x99 as the fitted s-curve parameters of area used for PV. This will be entered as inputs for Emissions_Simulation_Script.py.

Packages Required

The packages required for Singapore_cumulative_PV_s_curve_fitting_script.py to work are given in DEPENDENCIES.txt.

Files (Data input)
pv_data_filename : filename (+ location) of the .csv file with quaterly cumulative installed PV capacity in MWp.
total_electricity_gen_filename : filename (+ location) of the .csv file with the total registered electricity generation capacity in MW.


Constants (Simulation parameters)

N_PLOT : Number of quarters to plot
N_SOLAR_LIFE : Lifespan of solar panels in years

CURRENT_PV_EFFICIENCY : efficiency of current (and historical) PV. 100% efficiency has a value of 1.
FINAL_PV_EFFICIENCY : maximum efficiency of PV panels.
PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE : the annual increase in PV efficiency. New efficiency = efficiency at the beginning + number of years * PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE 
PV_DEGREDATION_RATE : annual PV degradation rate. New efficiency of PV = efficiency at installation * (1 - number of years * PV_DEGREDATION_RATE )

CURRENT_PV_EFFICIENCY, FINAL_PV_EFFICIENCY , PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE and PV_DEGREDATION_RATE  with _1 or _2 suffix: These are values for scenarios 1 and 2. In the paper, these are set Single Junction and Perovskite PV, respectively.


MAX_AREA : Maximum area that is available for the installation of PV in km^2. This can be changed if different amount of maximum land can be used for PV deployment.
MAX_PV_CAPACITY : Intermediate calculation of maximum PV capacity assuming an efficiency of CURRENT_PV_EFFICIENCY, in MWp
PVOUT : Estimated annual output of PV for the local PV location. This is in kWh/kWp per year. Set to 1300 for Singapore. This should be set to the value, or average value of the location in which the PV panels are installed.


############## Emissions_Simulation_Script.py

Emissions_Simulation_Script.py uses estimates of local PV (from Singapore_cumulative_PV_s_curve_fitting_script.py or otherwise), electrification fractions and rates, and external RE penetration fractions and rates to simulate cumulative GHG emissions. This script plots 3 figures. It takes a .csv file with primary energy demands as an input.

Packages Required

The packages required for Emissions_Simulation_Script.py to work are given in DEPENDENCIES.txt.


Files (Data input)
primary_demand_file : filename (+ location) of the .csv file with various historical primary energy demands: electricity (TWh), low temperature heat (TWh), high temperature heat (TWh), and transport (Tm)


Constants (Simulation parameters)
All times in the simulation parameters assume a start year of 2016. i.e. year 0 means 2016; year 5 means 2021.

USE_PEROVSKITE : Set to 1 or 0 to use or not use perovskite PV, respectively.
USE_EXTERNAL : Set to 1 or 0 to use or not use external solar, respectively.

N_SOLAR_LIFE : lifespan of solar panel (years). i.e. time after which they need to be replaced.
CURRENT_PV_EFFICIENCY : Efficiency of current (and historical) PV panels
FINAL_PV_EFFICIENCY : Peak efficiency of PV panels if the eiffiency of newly installed panels is rising each year.

PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE : the annual increase in PV efficiency. New efficiency = CURRENT_PV_EFFICIENCY  + number of years * PV_EFFICIENCY_ANNUAL_LINEAR_GROWTH_RATE
PV_DEGREDATION_RATE : annual PV degradation rate. New efficiency of PV = efficiency at installation * (1 - number of years * PV_DEGREDATION_RATE )
MAX_AREA : Maximum area that is available for the installation of PV in km^2. This can be changed if different amount of maximum land can be used for PV deployment.
MAX_PV_CAPACITY : Intermediate calculation of maximum PV capacity assuming an efficiency of CURRENT_PV_EFFICIENCY, in MWp
PVOUT : Estimated annual output of PV for the local PV location. This is in kWh/kWp per year. Set to 1300 for Singapore. This should be set to the value, or average value of the location in which the PV panels are installed.

FRACTIONAL_RE_PENETRATION_l : The maximum fraction of external RE penetration. This is the ratio of the external RE output divided by the net local electrical demand (electrical demand minus local RE electricity output). It is set to 99% instead of 100% in the paper, but may be set to 100%.

LOW_T_HEAT_ELECTRIFICATION_L : This is the maximum electrification of low temperature heating for the scenarios that have low temperature heating electrification.
LOW_T_HEAT_ELECTRIFICATION_X1 : This is the "start time" for low temeperature heating electrification. It is the time at which electrification is 1% of its maximum possible value.
LOW_T_HEAT_ELECTRIFICATION_X99 : This it the "end time" for low temperature heating electrification. It is the time at which electrification is 99% of its maximum possible value. 

HIGH_T_HEAT_ELECTRIFICATION_L : This is the maximum electrification of high temperature heating for the scenarios that have high temperature heating electrification.

if USE_EXTERNAL ==1: The following two are used if there is external RE
	HIGH_T_HEAT_ELECTRIFICATION_X1 : This is the "start time" for high temeperature heating electrification. It is the time at which electrification is 1% of its maximum possible value.
	HIGH_T_HEAT_ELECTRIFICATION_X99 : This it the "end time" for high temperature heating electrification. It is the time at which electrification is 99% of its maximum possible value. 
else: The following two are used if there is no external RE. They are set differently for scenario testing in the paper, but they could be set to be the same.
	HIGH_T_HEAT_ELECTRIFICATION_X1 : This is the "start time" for high temeperature heating electrification. It is the time at which electrification is 1% of its maximum possible value.
	HIGH_T_HEAT_ELECTRIFICATION_X99 : This it the "end time" for high temperature heating electrification. It is the time at which electrification is 99% of its maximum possible value. 

TRANSPORT_ELECTRIFICATION_L : This is the maximum electrification of transport for the scenarios that have transport electrification.
TRANSPORT_ELECTRIFICATION_X1 :This is the "start time" for transport electrification. It is the time at which electrification is 1% of its maximum possible value.
TRANSPORT_ELECTRIFICATION_X99 : This it the "end time" for transport  electrification. It is the time at which electrification is 99% of its maximum possible value. 

ABSOLUTE_RE_PENETRATION_L : This is the maximum amount of local RE output. This should be obtained as an output of the s-curve fitting done using the Singapore_cumulative_PV_s_curve_fitting_script.py script.
ABSOLUTE_RE_PENETRATION_X1 : This is the "start time" of local PV penetration. It is the time at which the local RE output is 1% of its maximum. This should be obtained as an output of the s-curve fitting done using the Singapore_cumulative_PV_s_curve_fitting_script.py script.
ABSOLUTE_RE_PENETRATION_X99 :   This is the "end time" of local PV penetration. It is the time at which the local RE output is 99% of its maximum. This should be obtained as an output of the s-curve fitting done using the Singapore_cumulative_PV_s_curve_fitting_script.py script.
